
'''
Program: hangman.py
Author: Hugo Alvarez Valdivia
Last date modified: 12/06/2023
This module includes a function that
returns the path of the image asset
that corresponds to the current 
hangman stage of the game. This is
a simulation of a switch statement
using a dictionary. If python version
is 3.10 or later the "match" statement
can be used.
'''

def getHangmanStage(stage):
    switch = {
        1: "./imgs/1.png",
        2: "./imgs/2.png",
        3: "./imgs/3.png",
        4: "./imgs/4.png",
        5: "./imgs/5.png",
        6: "./imgs/6.png",
        7: "./imgs/7.png"
    }
    
    return switch.get(stage)