
'''
Program: wordapi.py
Author: Hugo Alvarez Valdivia
Last date modified: 12/06/2023
This module includes a function that
returns a list of random words acquired
utilizing an http request that hits
the https://random-word-api.vercel.app/ 
endpoint. All credit goes to them for
their API implementation.
'''

import requests
import random

def getRandomWords(number, difficulty):
    """ This function calls an API via HTTPS request and
        creates a given number of random words.

    Args:
        num (int): Number of words to generate

    Returns:
        array: Random words array
    """
    
    # Implementing a difficulty setting to game based around word length
    if (difficulty == 1):
        length = random.randint(3, 4) 
    elif (difficulty == 2):
        length = random.randint(5,7)
    else:
        length = random.randint(8,9)
    
    url = 'https://random-word-api.vercel.app/api?words=' + str(number) + '&length=' + str(length)
    response = requests.get(url)

    if response.status_code == 200:
        print('Request successful')
        
    else:
        print(f'Request failed with status code {response.status_code}')
        
    words = response.text
    
    # Removing unnecessary chars from response body
    words = words.replace('"', '')
    words = words.replace("[", "")
    words = words.replace("]", "")
    
    # Storing them in array form
    words = words.split(',')
    
    return words