# Hangman-Python

## Getting Started

This program requires the [requests](https://pypi.org/project/requests/) and [Pillow](https://pypi.org/project/Pillow/) modules, as well as [pytest](https://pypi.org/project/pytest/) to run the unit tests:

```bash
pip install requests
```
```bash
pip install Pillow
```
```bash
pip install pytest
```

To run the game cd in the game directory and run using python.

Windows:

```console
cd \path\to\game\directory\
```
```console
python .\main.py
```

Unix/MacOS:

```bash
cd /path/to/game/directory/
```
```bash
python3 ./main.py
```

To run the tests:
```bash
cd /path/to/game/directory/
```
```bash
python3 ./run_tests.py
```