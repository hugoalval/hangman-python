
'''
Program: main.py
Author: Hugo Alvarez Valdivia
Last date modified: 12/06/2023
This file contains the logic for
the hangman game, as well as all the
GUI code.
'''

import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import random
import sys
from class_definitions.wordapi import getRandomWords
from class_definitions.hangman import getHangmanStage

class HangmanGame:
    
    """
    HangmanGame class

    Attributes:
        master (tk.Tk): The master Tkinter window.
        difficulty (str): The difficulty level of the game.
        word_list (list): List of words based on the difficulty.
        secret_word (str): The word to be guessed.
        guess_word (list): The current state of the guessed word.
        attempts_left (int): Number of attempts left for the player.

    Methods:
        get_word_list(difficulty): Get the list of words based on difficulty.
        create_image_frame(): Create the frame for displaying hangman images.
        create_word_label(): Create the label for displaying the guessed word.
        create_alphabet_buttons(): Create letter buttons for user input.
        make_guess(guess): Process a user's guess and update the game state.
        update_attempts_label(): Update hangman image and check game status.
        reset_buttons(): Reset the state of all letter buttons.
        reset_game(): Reset game variables and update the GUI for a new game.
    """
    
    def __init__(self, master, difficulty):
        """
        Class constructor to initialize a new hangman game.

        Args:
            master (tk.Tk): The master Tkinter window.
            difficulty (str): The difficulty level of the game.
        """
        self.master = master
        self.difficulty = difficulty
        self.word_list = self.get_word_list(self.difficulty)

        self.secret_word = random.choice(self.word_list).lower()
        self.guess_word = ['_' if char.isalpha() else char for char in self.secret_word]

        self.attempts_left = 6

        self.create_image_frame()
        self.create_word_label()

        self.create_alphabet_buttons()

    def get_word_list(self, difficulty):
        """
        Get the list of words based on the difficulty level. Utilizes the wordapi module
        to get random words from https://random-word-api.vercel.app. For more info check
        out wordapi.py under the class_definitions directory. Since the game uses a list
        of words and selects a random one from ther instead of making a http request every 
        time the game resets, I opted for constructing the request for a 20-word list.

        Args:
            difficulty (str): The difficulty level.

        Returns:
            list: List of words corresponding to the difficulty.
        """
        if difficulty == "easy":
            return getRandomWords(20,1)
        elif difficulty == "medium":
            return getRandomWords(20,2)
        elif difficulty == "hard":
            return getRandomWords(20,3)
        else:
            # In theory this would be reached by the GUI since we are using buttons, but left
            # it just in case.
            messagebox.showerror("Invalid Difficulty", "Please select a valid difficulty level.")
            self.master.destroy()

    def create_image_frame(self):
        """Creates the frame for displaying hangman images."""
        self.image_frame = tk.Frame(self.master)
        self.image_frame.pack(pady=10)

        # These assets were made by me, they're found inside the /imgs directory
        self.hangman_images = [ImageTk.PhotoImage(Image.open(getHangmanStage(i))) for i in range(1,8)]

        self.hangman_label = tk.Label(self.image_frame, image=self.hangman_images[0])
        self.hangman_label.pack()

    def create_word_label(self):
        """Creates the label for displaying the guessed word."""
        self.word_label = tk.Label(self.master, text=" ".join(self.guess_word), font=("Helvetica", 16))
        self.word_label.pack(pady=10)

    def create_alphabet_buttons(self):
        """Creates letter buttons for user input."""
        alphabet_frame = tk.Frame(self.master)
        alphabet_frame.pack()

        rows = 2
        cols = 13

        # To keep track of letter buttons
        self.buttons = []  

        # Instead of adding buttons one by one, decided to use a loop.
        for idx, char in enumerate("abcdefghijklmnopqrstuvwxyz"):
            row_idx = idx // cols
            col_idx = idx % cols

            button = tk.Button(alphabet_frame, text=char.upper(), command=lambda c=char: self.make_guess(c), width=3)
            button.grid(row=row_idx, column=col_idx, padx=3, pady=3)
            
            self.buttons.append(button)

        # Quit button to close game fully
        quit_button = tk.Button(self.master, text="Quit", command=sys.exit)
        quit_button.pack(pady=10)

    def make_guess(self, guess):
        """
        Process a user's guess and update the game state.

        Args:
            guess (str): The guessed letter.
        """
        if guess.isalpha() and len(guess) == 1:
            # Disable the button
            for button in self.buttons:
                if button.cget("text").lower() == guess:
                    button.config(state=tk.DISABLED)

            if guess in self.secret_word:
                # Update the guessed word
                for i, char in enumerate(self.secret_word):
                    if char == guess:
                        self.guess_word[i] = guess

                self.word_label.config(text=" ".join(self.guess_word))

                # Check if the player has guessed the entire word
                if "_" not in self.guess_word:
                    messagebox.showinfo("Congratulations!", "You guessed the word!")
                    self.reset_game()

            else:
                self.attempts_left -= 1
                self.update_image_stage()

                if self.attempts_left == 0:
                    messagebox.showinfo("Game Over", f"The word was '{self.secret_word}'. You lost!")
                    self.reset_game()
        else:
            # In theory the game would never reach this stage since I'm using buttons instead of text
            # input, but I left it just in case.
            messagebox.showwarning("Invalid Guess", "Please enter a single alphabetical character.")

    def update_image_stage(self):
        """Update hangman image and check game status. I opted for creating my own assets instead
           of using tkinter's canvas and drawing. Images were created in Paint for simplicity."""
        self.hangman_label.config(image=self.hangman_images[6 - self.attempts_left])
        self.hangman_label.image = self.hangman_images[6 - self.attempts_left]

        if self.attempts_left == 0:
            messagebox.showinfo("Game Over", f"The word was '{self.secret_word}'. You lost!")
            self.reset_game()

    def reset_buttons(self):
        """Reset the state of all letter buttons, since every game greys-out the buttons that
           have already been selected. This resets that behavior to the norm after a gameover."""
        for button in self.buttons:
            button.config(state=tk.NORMAL)

    def reset_game(self):
        """Reset game variables and update the GUI for a new game."""
        self.secret_word = random.choice(self.word_list).lower()
        self.guess_word = ['_' if char.isalpha() else char for char in self.secret_word]
        self.attempts_left = 6

        # Update GUI
        self.word_label.config(text=" ".join(self.guess_word))

        # Reset hangman image
        self.hangman_label.config(image=self.hangman_images[0])
        self.hangman_label.image = self.hangman_images[0]

        # Remove previous attempts label
        for widget in self.master.winfo_children():
            if isinstance(widget, tk.Label) and "Attempts Left:" in widget.cget("text"):
                widget.destroy()

        # Reset buttons
        self.reset_buttons()

def choose_difficulty(difficulty_var, main_frame):
    """
    This handles the difficulty selection window.

    Args:
        difficulty_var (tk.StringVar): The variable to store the difficulty.
        main_frame (tk.Frame): The main frame containing the difficulty choices.
    """
    main_frame.destroy()

    difficulty_frame = tk.Frame(root)
    difficulty_frame.pack(expand=True, fill='both')

    label = tk.Label(difficulty_frame, text="Select a difficulty level:")
    label.pack(pady=10)

    easy_button = tk.Button(difficulty_frame, text="Easy", command=lambda: start_game("easy", difficulty_frame))
    easy_button.pack(pady=5)

    medium_button = tk.Button(difficulty_frame, text="Medium", command=lambda: start_game("medium", difficulty_frame))
    medium_button.pack(pady=5)

    hard_button = tk.Button(difficulty_frame, text="Hard", command=lambda: start_game("hard", difficulty_frame))
    hard_button.pack(pady=5)

def start_game(difficulty, difficulty_frame):
    """
    Start a new game.

    Args:
        difficulty (str): The selected difficulty level.
        difficulty_frame (tk.Frame): The frame containing the difficulty choices.
    """
    difficulty_frame.destroy()
    hangman_frame = tk.Frame(root)
    hangman_frame.pack(expand=True, fill='both')
    hangman_game = HangmanGame(hangman_frame, difficulty)


"""Main code to initialize the Hangman game class."""
root = tk.Tk()
root.title("Hangman Game")

main_frame = tk.Frame(root)
main_frame.pack(expand=True, fill='both')

difficulty_var = tk.StringVar()

choose_difficulty_button = tk.Button(main_frame, text="Choose Difficulty", command=lambda: choose_difficulty(difficulty_var, main_frame))
choose_difficulty_button.pack(pady=50)

root.mainloop()
