
'''
Program: test_hangman.py
Author: Hugo Alvarez Valdivia
Last date modified: 12/06/2023
Unit testing using pytest for
the hangman stage getter (returns
the image path depending on the stage
that is sent.)
'''

from class_definitions.hangman import getHangmanStage

def test_hangman_stage():
    stage = getHangmanStage(1)
    assert stage == "./imgs/1.png"
    stage = getHangmanStage(2)
    assert stage == "./imgs/2.png"
    stage = getHangmanStage(3)
    assert stage == "./imgs/3.png"
    stage = getHangmanStage(4)
    assert stage == "./imgs/4.png"
    stage = getHangmanStage(5)
    assert stage == "./imgs/5.png"
    stage = getHangmanStage(6)
    assert stage == "./imgs/6.png"
    stage = getHangmanStage(7)
    assert stage == "./imgs/7.png"