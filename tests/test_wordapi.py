
'''
Program: test_wordapi.py
Author: Hugo Alvarez Valdivia
Last date modified: 12/06/2023
Unit testing module that verifies
the correct behavior for the
getRandomWords() function that
creates http requests, should be 
a little slower than usual
because of this.
'''

from class_definitions.wordapi import getRandomWords

def test_one_word():
    # Getting one word
    words = getRandomWords(1,1)
    #Testing that only one word was returned
    assert len(words) == 1
    
def test_word_list():
    # Getting list of 5 words
    words = getRandomWords(5,1)
    assert len(words) == 5
    
def test_word_length_easy():
    words = getRandomWords(1,1)
    # Testing that the length of the returned word actually falls
    # under the "easy" difficulty. i.e., the word has a random 
    # length between 3 and 5 letters.
    assert (len(words[0]) >= 3 and len(words[0]) <= 4)
    
def test_word_length_medium():
    words = getRandomWords(1,2)
    # Testing that the length of the returned word actually falls
    # under the "medium" difficulty. i.e., the word has a random 
    # length between 5 and 8 letters.
    assert (len(words[0]) >= 5 and len(words[0]) <= 7)
    
def test_word_length_hard():
    words = getRandomWords(1,3)
    # Testing that the length of the returned word actually falls
    # under the "hard" difficulty. i.e., the word has a random 
    # length larger than 9
    assert (len(words[0]) >= 8)